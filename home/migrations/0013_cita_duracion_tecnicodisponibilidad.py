# Generated by Django 4.2.7 on 2023-11-26 06:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0012_alter_tipomaquina_nombre'),
    ]

    operations = [
        migrations.AddField(
            model_name='cita',
            name='Duracion',
            field=models.PositiveIntegerField(default=3, help_text='Duración de la cita en horas'),
        ),
        migrations.CreateModel(
            name='TecnicoDisponibilidad',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('Fecha', models.DateField()),
                ('Hora', models.CharField(max_length=10)),
                ('Disponible', models.BooleanField(default=True)),
                ('Tecnico', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='home.tecnico')),
            ],
        ),
    ]
