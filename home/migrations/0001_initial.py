# Generated by Django 4.2.7 on 2023-11-19 02:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='usuarios',
            fields=[
                ('Numero', models.AutoField(editable=False, primary_key=True, serialize=False, unique=True)),
                ('Nombre', models.CharField(max_length=50, null=True)),
                ('NumTel', models.CharField(max_length=15)),
                ('ContNomPila', models.CharField(max_length=30)),
                ('ContPrimApell', models.CharField(max_length=30)),
                ('ContSegApell', models.CharField(blank=True, max_length=30, null=True)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
