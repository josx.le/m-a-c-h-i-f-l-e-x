from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.urls import reverse_lazy, reverse
from .forms import *
from .models import usuarios, Cita, Servicio

from django.http import HttpResponse
from datetime import datetime


## I N D E X  A N D   L O G I N


class Index(generic.View):
    template_name = "home/index.html"
    context = {}
    form = LoginForm

    def get(self, request, *args, **kwargs):
        self.context = {
            "form": self.form
        }
        return render(request, self.template_name, self.context)

    def post(self, request):
        username = request.POST["Username"]
        password = request.POST["Contraseña"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/Nosotros/")
        else:
            return redirect("/")
        

## S I G N U P


class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:Nosotros")

    def form_valid(self, form):
        response = super().form_valid(form)
        user = form.save()
        if user is not None:

            username = form.cleaned_data.get("username")
            password1 = form.cleaned_data.get("password1")
            use = authenticate(self.request, username=username, password=password1)

            if use is not None:
                login(self.request, use)
                guardar = usuarios(user=user, Nombre = form.cleaned_data['Nombre'], NumTel = form.cleaned_data['NumTel'], ContNomPila = form.cleaned_data['ContNomPila'], ContPrimApell = form.cleaned_data['ContPrimApell'], ContSegApell = form.cleaned_data['ContSegApell'])
                guardar.save()

        return response
    


## N O S O T R O S



class Nosotros(LoginRequiredMixin, generic.View):
    template_name = "home/SobreNosotros.html"
    context = {}
    login_url = '/'

    def get(self, request, *args, **kwargs):
        self.context = {}
        return render(request, self.template_name, self.context)
    

## L O G O U T


class LogOut(LoginRequiredMixin, generic.View):
    login_url = '/'
    
    def get(self, request):
        logout(request)
        return redirect("/")
    
## D E T A I L

class List(generic.View):
    template_name = "core/list.html"
    context = {}
    model= usuarios
    def get(self, request, pk):
        self.context = {
            "usuarios" : User.objects.get(id=pk)
        }
        return render(request, self.template_name, self.context)
    

## U P D A T E

class Update(generic.UpdateView):
    template_name = "core/update.html"
    model = usuarios
    form_class = UpdateClienteForm
    success_url = reverse_lazy("home:lista")

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("home:lista", kwargs={"pk":pk})


##  L I S T A   M A Q U I N A S

class Lmaquinas(generic.View):
    template_name = "home/lmaquinas.html"

    def get(self, request, *args, **kwargs):
        queryset = Maquina.objects.all()
        self.context = {
            "Maquina": queryset
        }
        return render(request, self.template_name,self.context)
    

class DetailMaquina(generic.DetailView):
    template_name = "core/detailMaquina.html"
    model = Maquina

    
class CreateMaquina(generic.CreateView):
    template_name = "core/createMaquina.html"
    model = Maquina
    form_class = MaquinaForm
    success_url = reverse_lazy("home:maquinas")

class UpdateMaquinas(generic.UpdateView):
    template_name = "core/Updatemaquinas.html"
    model = Maquina
    form_class = UpdateMaquinaForm
    success_url = reverse_lazy("home:maquinas")

class DeleteMaquinas(generic.DeleteView):
    template_name = "core/deletemaquinas.html"
    model = Maquina
    success_url = reverse_lazy("home:maquinas")
    
## H I S T O R I A L

class Historial(generic.View):
    template_name = "core/Historial.html"
    context = {}

    def get(self, request, *args, **kwargs):
        usuario = usuarios.objects.get(user=request.user)
        queryset = Cita.objects.filter(Usuario=usuario)

        self.context = {
            "Cita": queryset
        }
        return render(request, self.template_name, self.context)

##  L I S T A  S E R V I C I O S

class List2(generic.View):
    template_name = "core/list2.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Servicio.objects.all()
        self.context = {
            "Servicio": queryset
        }
        return render(request, self.template_name,self.context)
    
class DetailServicio(generic.DetailView):
    template_name = "core/detail2.html"
    model = Servicio

class CreateServicio(generic.CreateView):
    template_name = "core/create2.html"
    model = Servicio
    form_class = ServiciosForm
    success_url = reverse_lazy("home:list2")

class UpdateServicios(generic.UpdateView):
    template_name = "core/Update2.html"
    model = Servicio
    form_class = UpdateServiciosForm
    success_url = reverse_lazy("home:list2")

class DeleteServicios(generic.DeleteView):
    template_name = "core/delete2.html"
    model = Servicio
    success_url = reverse_lazy("home:list2")



##  L I S T A  S E R V C A M B



class ListServCamb(generic.View):
    template_name = "core/Listservcamb.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = ServCambio.objects.all()
        self.context = {
            "ServCambio": queryset
        }
        return render(request, self.template_name,self.context)
    
class DetailServCamb(generic.DetailView):
    template_name = "core/detailServCamb.html"
    model = ServCambio

class CreateServCamb(generic.CreateView):
    template_name = "core/createServCamb.html"
    model = ServCambio
    form_class = ServCambForm
    success_url = reverse_lazy("home:listaservcamb")

class UpdateServCamb(generic.UpdateView):
    template_name = "core/UpdateServCamb.html"
    model = ServCambio
    form_class = UpdateServCambForm
    success_url = reverse_lazy("home:listaservcamb")

class DeleteServCamb(generic.DeleteView):
    template_name = "core/deleteServCamb.html"
    model = ServCambio
    success_url = reverse_lazy("home:listaservcamb")



##  C A M B I O S

class ListaCambios(generic.View):
    template_name = "core/ListaCambios.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Cambio.objects.all()
        self.context = {
            "Cambio": queryset
        }
        return render(request, self.template_name,self.context)
    
class DetailCambios(generic.DetailView):
    template_name = "core/detailCambios.html"
    model = Cambio

class CreateCambios(generic.CreateView):
    template_name = "core/createCambios.html"
    model = Cambio
    form_class = CambiosForm
    success_url = reverse_lazy("home:listacambios")

class UpdateCambios(generic.UpdateView):
    template_name = "core/UpdateCambios.html"
    model = Cambio
    form_class = UpdateCambiosForm
    success_url = reverse_lazy("home:listacambios")

class DeleteCambios(generic.DeleteView):
    template_name = "core/deleteCambios.html"
    model = Cambio
    success_url = reverse_lazy("home:listacambios")



###  T É C N I C O S


class ListaTecnicos(generic.View):
    template_name = "core/ListaTecnicos.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Tecnico.objects.all()
        self.context = {
            "Tecnico": queryset
        }
        return render(request, self.template_name,self.context)

class DetailTecnicos(generic.DetailView):
    template_name = "core/detailTecnicos.html"
    model = Tecnico


class CreateTecnicos(generic.CreateView):
    template_name = "core/createTecnicos.html"
    model = Tecnico
    form_class = TecnicosForm
    success_url = reverse_lazy("home:ListaTecnicos")


class UpdateTecnicos(generic.UpdateView):
    template_name = "core/UpdateTecnicos.html"
    model = Tecnico
    form_class = UpdateTecnicosForm
    success_url = reverse_lazy("home:ListaTecnicos")


class DeleteTecnicos(generic.DeleteView):
    template_name = "core/deleteTecnicos.html"
    model = Tecnico
    success_url = reverse_lazy("home:ListaTecnicos")


## C I T A S   C R U D  1 / 2


class ListaCitas(generic.View):
    template_name = "core/ListaCitas.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Cita.objects.all()
        self.context = {
            "Cita": queryset
        }
        return render(request, self.template_name,self.context)


class DetailCita(generic.DetailView):
    template_name = "core/detailCita.html"
    model = Cita


class DeleteCita(generic.DeleteView):
    template_name = "core/deleteCita.html"
    model = Cita
    success_url = reverse_lazy("home:listacitas")


## L I S T A   D E    U S U A R I O S  3/4

class ListaUsers(generic.View):
    template_name = "core/ListaUsers.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = usuarios.objects.all()
        self.context = {
            "usuarios": queryset
        }
        return render(request, self.template_name,self.context)


class DetailUsers(generic.DetailView):
    template_name = "core/detailUsers.html"
    model = usuarios
    

class UpdateUsers(generic.UpdateView):
    template_name = "core/UpdateUsers.html"
    model = usuarios
    form_class = UpdateUsersForm
    success_url = reverse_lazy("home:listausers")

class DeleteUsers(generic.DeleteView):
    template_name = "core/deleteUsers.html"
    model = usuarios
    success_url = reverse_lazy("home:listausers")

##  C I T A 


class CitaView(generic.View):
    template_name = "home/confirmar.html"
    model = Cita
    tecnico = Tecnico.objects.filter(Status=True).first()
    success_url = reverse_lazy("home:Confirmar")
    

    def get(self, request, *args, **kwargs):
        
        fecha_seleccionada = request.GET.get('Fecha')
        print("@@@@", fecha_seleccionada)
        print(type(fecha_seleccionada))
        fecha_seleccionada = datetime.strptime(fecha_seleccionada, "%Y-%m-%d").date()

        hora_seleccionada = request.GET.get('Hora')

        print("Fecha seleccionada:", fecha_seleccionada)
        print(type(fecha_seleccionada))

        print("Hora seleccionada:", hora_seleccionada)
        print(type(fecha_seleccionada))


        # Obtén un técnico disponible (Status=True)
        #tecnico = Tecnico.objects.filter(Status=True).first()
        print("$$$$$$", self.tecnico)
        
        

        context = {
            'Fecha': fecha_seleccionada,
            'Hora': hora_seleccionada,
            'Tecnico': self.tecnico,
        }

        return render(request, self.template_name, context)

    def post(self, request):
        fecha_seleccionada = request.POST['Fecha']
        hora_seleccionada = request.POST.get("Hora", '')

        try:
            self.tecnico = Tecnico.objects.filter(Status=True).first()

            if self.tecnico is not None and self.tecnico.Status is True:
                print("Técnico obtenido:", self.tecnico)

                self.tecnico.Status = False
                self.tecnico.save()

            else:
                return HttpResponse("Error: Técnico no válido o no disponible")
        except Tecnico.DoesNotExist:
            return HttpResponse("Error: Técnico no válido o no disponible")
        # Se guarda en la base de datos
        user = usuarios.objects.get(user=request.user)
        nueva_cita = Cita.objects.create(
            Fecha=fecha_seleccionada,
            Hora=hora_seleccionada,
            Tecnico=self.tecnico,
            Usuario=user,
        )

        # Pasa los datos a home confirmacion
        context = {
            'Fecha': fecha_seleccionada,
            'Hora': hora_seleccionada,
            'Tecnico': self.tecnico,
            'Usuario': user,
        }

        return render(request, "home/confirmar.html", context)