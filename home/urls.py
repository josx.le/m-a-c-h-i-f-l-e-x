from django.urls import path
from home import views


app_name ="home"
urlpatterns = [
    ##L O B B Y
    path('', views.Index.as_view(), name="index"),
    path('Nosotros/', views.Nosotros.as_view(), name="Nosotros"),
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('logout/', views.LogOut.as_view(), name="logout"),
    ## U S E R
    path('detail/usuarios/<int:pk>/', views.List.as_view(), name="lista"),
    path('update/usuarios/<int:pk>/', views.Update.as_view(), name= "update"),
    ## M A C H I N E
    path('maquinas/', views.Lmaquinas.as_view(), name="maquinas"),
    path('detail/maquinas/<str:pk>/', views.DetailMaquina.as_view(), name= "detailmaquina"),
    path('create/maquinas/',views.CreateMaquina.as_view(), name="createmaquina"),
    path('update/maquinas/<str:pk>/', views.UpdateMaquinas.as_view(), name= "Updatemaquinas"),
    path('delete/maquina/<str:pk>/', views.DeleteMaquinas.as_view(), name= "deletemaquinas"),
    ## T E C H N I C I A N S
    path('Tecnicos/', views.ListaTecnicos.as_view(), name="ListaTecnicos"),
    path('detail/Tecnicos/<int:pk>/', views.DetailTecnicos.as_view(), name= "detailtecnicos"),
    path('create/Tecnicos/',views.CreateTecnicos.as_view(), name="createtecnicos"),
    path('update/Tecnicos/<int:pk>/', views.UpdateTecnicos.as_view(), name= "Updatetecnicos"),
    path('delete/Tecnicos/<int:pk>/', views.DeleteTecnicos.as_view(), name= "deletetecnicos"),
    ## U S 
    path('nosotros/historial/',views.Historial.as_view(), name="historial"),
    ## S E R V I C E
    path('list2/Servicios/',views.List2.as_view(), name="list2"),
    path('detail/Servicios/<int:pk>/', views.DetailServicio.as_view(), name= "detail2"),
    path('create/Servicios/',views.CreateServicio.as_view(), name="create2"),
    path('update/Servicios/<int:pk>/', views.UpdateServicios.as_view(), name= "Update2"),
    path('delete/Servicios/<int:pk>/', views.DeleteServicios.as_view(), name= "delete2"),
    ## C H A N G E  S E R V I C E
    path('nosotros/ServCamb', views.ListServCamb.as_view(), name="listaservcamb"),
    path('detail/ServCamb/<int:pk>/', views.DetailServCamb.as_view(), name= "DetailServCamb"),
    path('create/ServCamb/',views.CreateServCamb.as_view(), name="createServCamb"),
    path('update/ServCamb/<int:pk>/', views.UpdateServCamb.as_view(), name= "UpdateServCamb"),
    path('delete/ServCamb/<int:pk>/', views.DeleteServCamb.as_view(), name= "deleteServCamb"),
    ## C H A N G E S
    path('nosotros/Cambios', views.ListaCambios.as_view(), name="listacambios"),
    path('detail/Cambios/<str:pk>/', views.DetailCambios.as_view(), name= "DetailCambios"),
    path('create/Cambios/',views.CreateCambios.as_view(), name="createCambios"),
    path('update/Cambios/<str:pk>/', views.UpdateCambios.as_view(), name= "UpdateCambios"),
    path('delete/Cambios/<str:pk>/', views.DeleteCambios.as_view(), name= "deleteCambios"),
    ## A P P O I N T M E N T S
    path('nosotros/Citas', views.ListaCitas.as_view(), name="listacitas"),
     path('detail/Citas/<int:pk>/', views.DetailCita.as_view(), name= "DetailCita"),
     path('delete/Citas/<int:pk>/', views.DeleteCita.as_view(), name= "deleteCitas"),
     ## U S E R S
     path('nosotros/users', views.ListaUsers.as_view(), name="listausers"),
     path('detail/users/<int:pk>/', views.DetailUsers.as_view(), name= "DetailUsers"),
     path('update/users/<int:pk>/', views.UpdateUsers.as_view(), name= "UpdateUsers"),
     path('delete/users/<int:pk>/', views.DeleteUsers.as_view(), name= "deleteUsers"),
    ## C O N F I R M
    path('Confirmar/',views.CitaView.as_view(), name="Confirmar"),
]